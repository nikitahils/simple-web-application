package controller;

import dao.StudentFacadeLocal;
import java.io.IOException;
import java.util.Collection;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.MyUtil;

/**
 *
 * @author hilshils
 * Данный сервлет предназначен для взаимодействия с пользователем
 * на странице (table.jsp), где производится отображение таблицы данных.
 * Здесь выполняется поиск необходимых данных для jsp-страницы, 
 * которые передаются ей в качестве атрибута, 
 * а так же реализована поддержка возможности удаления данных из таблицы.
 * 
 */
@WebServlet(name = "TableStudentInfoServlet", urlPatterns = {"/TableStudentInfoServlet"})
public class TableStudentInfoServlet extends HttpServlet {

    @EJB
    private StudentFacadeLocal studentDao;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String action = request.getParameter("action");
        String type   = request.getParameter("type");
        String query  = request.getParameter("query");
        
        if("delete".equals(action)) {
            String idString = request.getParameter("id");
            Long   id       = MyUtil.longFromString(idString);
            if(id != null) 
                studentDao.remove(studentDao.find(id));
            response.sendRedirect("TableStudentInfoServlet?type="+type+"&query="+query);
        } else {
            Collection result;
            if("group".equals(type) && (MyUtil.isNumeric(query) || "".equals(query))) {
                Long groupNumber = MyUtil.longFromString(query);
                result = (groupNumber != null)? studentDao.findByStudentGroupNumber(groupNumber) : studentDao.findByStudentWithoutGroup();
            } else {
                if((query != null) && (!"".equals(query)) && ("name".equals(type))) result = studentDao.findByNameSubstring(query);
                else result = studentDao.findAll();
            }
            
            request.setAttribute("students", result);
            request.getRequestDispatcher("table.jsp").forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
