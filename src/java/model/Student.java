package model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author hilshils
 *  Entity bean.
 *  Является объектным отображением сущности базы данных Student.
 *  Основные параметры:
 *   1.id       - уникальный идентификационный номер студента (primary key, unique, not null)
 *   2.name     - имя студента                                (null allowed)
 *   3.lastname - фамилия студента                            (null allowed)
 *   4.course   - номер курса, на котором учится студент      (null allowed)
 *   5.sGroup   - связь many-to-one с сущностью StudentGroup, группа, в которой учится студент (null allowed)
 * 
 */
@Entity
@Table(name = "Student") 
@NamedQueries({@NamedQuery(name = "Student.findByNameSubstring",       query = "SELECT s FROM Student s WHERE s.name LIKE :patternParameter"),
               @NamedQuery(name = "Student.findByStudentWithoutGroup", query = "SELECT s FROM Student s WHERE s.sGroup IS NULL"),
               @NamedQuery(name = "Student.findByStudentGroupNumber",  query = "SELECT s FROM Student s WHERE s.sGroup.groupNumber = :groupNumberParameter")})
public class Student implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "lastname")
    private String lastname;
    
    @Column(name = "course")
    private Long course;
    
    @JoinColumn(name = "sGroup", referencedColumnName = "groupNumber")
    @ManyToOne
    private StudentGroup sGroup;

    public Student() {
    }

    public Student(String name, String lastname, Long course, StudentGroup group) {
        this.name = name;
        this.lastname = lastname;
        this.course = course;
        this.sGroup = group;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Long getCourse() {
        return course;
    }

    public void setCourse(Long course) {
        this.course = course;
    }

    public StudentGroup getsGroup() {
        return sGroup;
    }

    public void setsGroup(StudentGroup sGroup) {
        this.sGroup = sGroup;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Student)) {
            return false;
        }
        Student other = (Student) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Student[ id=" + id + " ]";
    }
}
