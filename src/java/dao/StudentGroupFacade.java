package dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import static javax.ejb.TransactionAttributeType.REQUIRED;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.StudentGroup;

/**
 *
 * @author hilshils
 *  Session bean.
 *  Предоставляет реализацию прозрачного доступа к сущности базы данных - StudentGroup.
 *  Все сигнатуры методов взаимодействия с сущностью StudentGroup объявлены 
 *  в интерфейсе StudentGroupFacadeLocal.
 * 
 */
@Stateless
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@TransactionAttribute(value=REQUIRED)
public class StudentGroupFacade extends AbstractFacade<StudentGroup> implements StudentGroupFacadeLocal {
    @PersistenceContext(unitName = "WebAppPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public StudentGroupFacade() {
        super(StudentGroup.class);
    }
}
