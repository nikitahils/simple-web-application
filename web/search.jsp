<%-- 
    Document   : search
    Created on : Jul 16, 2013, 7:10:16 AM
    Author     : hilshils
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search student</title>
    </head>
    <body>
        <h1 align ="center">Search student</h1>
        <form method="POST" action='SearchStudentInfoServlet'>
        <table border="1" align='center'>
            <tr>
                <td>
                    <select name="type">
                        <option>name</option>
                        <option>group</option>
                    </select>
                </td>
                <td>
                    <input type="text" name='query'/>
                </td>
            </tr>
            <tr>
                <td colspan="2" align='center'> 
                    <input type="submit" value="search" name='action'/> 
                </td>
            </tr>
        </table>
        </form>
        <table align = "center">
            <tr>
                <td>
                     <a href='index.html'> <p>Start page</p></a>
                </td>
                <td>
                    <div style="width : 1em"></div>
                </td>
                <td>
                     <a href='TableStudentInfoServlet'> <p>Student info</p></a>
                </td>
            </tr>
        </table>
    </body>
</html>
