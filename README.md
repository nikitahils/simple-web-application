Данное веб приложение реализовано при использовании NetBeans 7 + GlassFish 4 + MySQL 5
================================
----------------------------------------
Проект развернут на Jelastic.
[Можно потестить тут!](http://env-8707868.jelastic.regruhosting.ru/index.html)
----------------------------------------
##Структура приложения:
  - Главная страница (index.html) есть возможность перехода на страницы поиска, просмотра таблицы студентов, добавления нового студента.
  - Страница поиска  (search.jsp) предоставляет выбор типа поиска - по имени (так же по подстроке в имени) или по группе (связь с другой таблицей).
     После нажатия подтверждения - осуществляется переход на страницу с таблицей студентов.
  - Таблица студентов (table.jsp) показывает список студентов, предоставляет возможность удалить/редактировать.
  - Страница редактирования (edit.jsp) предоставляет возможность добавить/редактировать студента в базе данных.
-----------------------------------------
##В основе реализации лежит MVC паттерн:
- ###Model - Entity & Session beans:
     - Student.java            (Entity beans)           
     - StudentGroup.java       (Entity beans)     
     - StudentFacade.java      (Session beans)
     - StudentGroupFacade.java (Session beans)
- ###View  - JSP & HTML pages
     - edit.jsp
     - table.jsp
     - search.jsp
	 - index.html
- ###Controller - Servlets
     - EditStudentInfoServlet.java
     - TableStudentInfoServlet.java
     - SearchStudentInfoServlet.java

##Так же проект содержит DAO-интерфейсы
- StudentFacadeLocal.java
- StudentGroupFacadeLocal.java

##И класс утилит
- MyUtil.java 

-----------------------------------------
##Структура базы данных:
- ###Сущность Student:
	 - id        уникальный идентификационный номер студента (primary key, unique, not null)
     - name      имя студента                                (null allowed)
     - lastname  фамилия студента                            (null allowed)
     - course    номер курса, на котором учится студент      (null allowed)
     - sGroup    связь many-to-one с сущностью StudentGroup  (null allowed)
- ###Сущность StudentGroup:
     - groupNumber  номер группы        (primary key, unique, not null)
     - facultyName  название факультета (null allowed)