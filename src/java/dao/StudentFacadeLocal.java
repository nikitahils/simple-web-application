package dao;

import java.util.List;
import javax.ejb.Local;
import model.Student;

/**
 *
 * @author hilshils
 * Локальный интерфейс для прозрачного 
 * доступа к таблице Student базы данных
 * 
 */
@Local
public interface StudentFacadeLocal {
     
    /**
     * Количество cтудентов в таблице
     * @return int количество
     */
    int  count();
     /**
     * Добаление нового студента в таблицу
     * @param  student - объект-студент
     */
    void create(Student student);
     /**
     * Удаление студента из таблицы
     * @param  student - объект-студент
     */
    void remove(Student student);
     /**
     * Изменение существующего студента в таблице
     * В случае отсутствия данного студента в таблице, производится добавление новго
     * @param  studentGroup - объект-группа
     */
    void edit(Student student);
     /**
     * Поиск студента в таблице по уникальному ключу
     * @param  id - объект-ключ (primary-key)
     */
    Student find(Object id);
     /**
     * Поиск студентов в таблице по подстроке в имени
     * @return List  - список студентов 
     */
    List<Student> findByNameSubstring(String name);
     /**
     * Поиск студентов в таблице по номеру группы
     * @return List  - список студентов 
     */
    List<Student> findByStudentGroupNumber(Long groupNumber);
     /**
     * Поиск студентов в таблице с отсутствующей связью с группой
     * @return List  - список студентов 
     */
    List<Student> findByStudentWithoutGroup();
     /**
     * Извлечение всех студентов из таблицы
     * @return List - список всех студентов
     */
    List<Student> findAll();
     /**
     * Извлечение студентов из таблицы в определенном диапазоне
     * @param  range - диапазон. массив из двух чисел, где 0ой элемент - начало, 1ый элемент - количество. 
     * @return List  - список студентов в диапазоне
     */
    List<Student> findRange(int[] range);
}
