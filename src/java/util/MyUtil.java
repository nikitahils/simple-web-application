package util;

/**
 *
 * @author hilshils
 */
public abstract class MyUtil {
    
    /**
    * Преобразование строки в число типа Long
    * 
    * @param  str - любая строка
    * @return Long число, если строка была корректной, иначе null.
    */
    public static Long longFromString(String str) {
        Long num = null;
        try {
            num = Long.valueOf(str);
        } catch (NumberFormatException e) {
            //do nothing
        }
        return num;
    }
    
    /**
    * Проверка - является ли строка числом.
    * 
    * @param str - любая строка
    * @return boolean значение true, если строка является числовым значением, false, если нет.
    */
    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }
}
