package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.StudentGroup;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2013-07-18T06:33:37")
@StaticMetamodel(Student.class)
public class Student_ { 

    public static volatile SingularAttribute<Student, Long> id;
    public static volatile SingularAttribute<Student, Long> course;
    public static volatile SingularAttribute<Student, String> name;
    public static volatile SingularAttribute<Student, String> lastname;
    public static volatile SingularAttribute<Student, StudentGroup> sGroup;

}