package model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author hilshils
 *  Entity bean.
 *  Является объектным отображением сущности базы данных StudentGroup.
 *  Основные параметры:
 *   1.groupNumber - номер группы (primary key, unique, not null)
 *   2.facultyName - название факультета, которому соответствует данный номер группы (null allowed)
 * 
 */
@Entity
@Table(name = "StudentGroup")
public class StudentGroup implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    @Column(name = "groupNumber")
    private Long groupNumber;
    
    @NotNull
    @Column(name = "facultyName")
    private String facultyName;
    
    @OneToMany(mappedBy = "sGroup")
    private Collection<Student> studentCollection;

    public StudentGroup() {
    }

    public Long getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(Long groupNumber) {
        this.groupNumber = groupNumber;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public Collection<Student> getStudentCollection() {
        return studentCollection;
    }

    public void setStudentCollection(Collection<Student> studentCollection) {
        this.studentCollection = studentCollection;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupNumber != null ? groupNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StudentGroup)) {
            return false;
        }
        StudentGroup other = (StudentGroup) object;
        if ((this.groupNumber == null && other.groupNumber != null) || (this.groupNumber != null && !this.groupNumber.equals(other.groupNumber))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.StudentGroup[ groupNumber=" + groupNumber + " ]";
    }
}
