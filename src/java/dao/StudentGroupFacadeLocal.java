package dao;

import java.util.List;
import javax.ejb.Local;
import model.StudentGroup;

/**
 *
 * @author hilshils
 * Локальный интерфейс для прозрачного 
 * доступа к таблице StudentGroup базы данных
 * 
 */
@Local
public interface StudentGroupFacadeLocal {
    
     /**
     * Количество групп в таблице
     * @return int количество
     */
    int  count();
     /**
     * Добаление новой группы в таблицу
     * @param  studentGroup - объект-группа
     */
    void create(StudentGroup studentGroup);
     /**
     * Удаление группы из таблицы
     * @param  studentGroup - объект-группа
     */
    void remove(StudentGroup studentGroup);
     /**
     * Изменение существующей группы в таблице
     * В случае отсутствия данной группы в таблице, производится добавление новой
     * @param  studentGroup - объект-группа
     */
    void edit(StudentGroup studentGroup);
     /**
     * Поиск группы в таблице по уникальному ключу
     * @param  id - объект-ключ (primary-key)
     */
    StudentGroup find(Object id);
     /**
     * Извлечение всех групп из таблицы
     * @return List - список всех групп
     */
    List<StudentGroup> findAll();
     /**
     * Извлечение групп из таблицы в определенном диапазоне
     * @param  range - диапазон. массив из двух чисел, где 0ой элемент - начало, 1ый элемент - количество. 
     * @return List  - список групп в диапазоне
     */
    List<StudentGroup> findRange(int[] range);  
}
