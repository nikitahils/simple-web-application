package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Student;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2013-07-18T06:33:37")
@StaticMetamodel(StudentGroup.class)
public class StudentGroup_ { 

    public static volatile SingularAttribute<StudentGroup, String> facultyName;
    public static volatile CollectionAttribute<StudentGroup, Student> studentCollection;
    public static volatile SingularAttribute<StudentGroup, Long> groupNumber;

}