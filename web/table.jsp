<%-- 
    Document   : index
    Created on : Jul 15, 2013, 6:42:07 PM
    Author     : hilshils
--%>
<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Student info</title>
    </head>
    <body>
        <h1 align ="center">Student info</h1>
        <table border = 1 align = "center">
            <thead>
                <tr>
                    <th>Student Id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Course</th>
                    <th>Group</th>
                    <th colspan=2>Action</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${students}" var="student">
                    <tr>
                        <td align ="center">${student.id}</td>
                        <td align ="center">${student.name}</td>
                        <td align ="center">${student.lastname}</td>
                        <td align ="center">${student.course}</td>
                        <td align ="center">${student.sGroup.groupNumber}</td>
                        <td><a href="EditStudentInfoServlet?id=${student.id}">Edit</a></td>
                        <td><a href="TableStudentInfoServlet?action=delete&id=${student.id}&type=${param.type}&query=${param.query}">Delete</a></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <table align = "center">
            <tr>
                <td>
                     <a href='index.html'> <p>Start page</p></a>
                </td>
                <td>
                    <div style="width : 1em"></div>
                </td>
                <td>
                     <a href='SearchStudentInfoServlet'> <p>Search student</p></a>
                </td>
                <td>
                    <div style="width : 1em"></div>
                </td>
                <td>
                     <a href='EditStudentInfoServlet'> <p>Add student</p></a>
                </td>
            </tr>
        </table>
    </body>
</html>