package dao;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import static javax.ejb.TransactionAttributeType.REQUIRED;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.Student;

/**
 *
 * @author hilshils
 *  Session bean.
 *  Предоставляет реализацию прозрачного доступа к сущности базы данных - Student.
 *  Все сигнатуры методов взаимодействия с сущностью Student объявлены 
 *  в интерфейсе StudentFacadeLocal. 
 * 
 */
@Stateless
@TransactionManagement(value=TransactionManagementType.CONTAINER)
@TransactionAttribute(value=REQUIRED)
public class StudentFacade extends AbstractFacade<Student> implements StudentFacadeLocal {
    @PersistenceContext(unitName = "WebAppPU")
    private EntityManager em;

    @Override
    public List<Student> findByNameSubstring(String name) {
        StringBuilder pattern = new StringBuilder("%").append(name).append("%");
        return em.createNamedQuery("Student.findByNameSubstring").setParameter("patternParameter", pattern.toString()).getResultList();
    }
    
    @Override
    public List<Student> findByStudentWithoutGroup() {
        return em.createNamedQuery("Student.findByStudentWithoutGroup").getResultList();
    }
    
    @Override
    public List<Student> findByStudentGroupNumber(Long groupNumber) {
        return em.createNamedQuery("Student.findByStudentGroupNumber").setParameter("groupNumberParameter", groupNumber).getResultList();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public StudentFacade() {
        super(Student.class);
    }
}
