<%-- 
    Document   : edit
    Created on : Jul 16, 2013, 6:53:50 PM
    Author     : hilshils
--%>

<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Update student info</title>
    </head>
    <body>
        <h1 align ="center"> Update student info</h1>
        <form method="POST" action='EditStudentInfoServlet'>
            <table border = 1 align = "center">
                <tr>
                    <td>User ID</td>
                    <td><input type="text" readonly="readonly" name="id"
                               value="${student.id}" /></td>
                </tr>
                <tr>
                    <td>First Name</td>
                    <td><input type="text" name="name"
                               value="${student.name}" /></td>
                </tr>
                <tr>
                    <td>Last Name</td>
                    <td><input type="text" name="lastname"
                               value="${student.lastname}" /></td>
                </tr>
                <tr>
                    <td>Course</td>
                    <td><input type="text" name="course" 
                               value="${student.course}" /></td>

                </tr>   
                <tr>
                    <td>Group</td>
                    <td>                    
                        <select name="groupNumber">
                            <c:forEach items="${groups}" var="group">
                                <option <c:if test="${group.equals(student.sGroup)}">selected</c:if>>
                                    ${group.groupNumber}
                                </option>
                            </c:forEach>
                                <option <c:if test="${student.sGroup == null}">selected</c:if>>
                                    <c:out value=""/>
                                </option>
                        </select>
                    </td>
                </tr> 
            </table>
            <br>
            <div style="text-align: center;">
                <input type="submit" name='action' value="save" style="width:150px"/>
            </div>
        </form>
        <table align = "center">
            <tr>
                <td>
                     <a href='index.html'> <p>Start page</p></a>
                </td>
                <td>
                    <div style="width : 1em"></div>
                </td>
                <td>
                     <a href='TableStudentInfoServlet'> <p>Student info</p></a>
                </td>
                <td>
                    <div style="width : 1em"></div>
                </td>
                <td>
                     <a href='SearchStudentInfoServlet'> <p>Search student</p></a>
                </td>
            </tr>
        </table>
    </body>
</html>

