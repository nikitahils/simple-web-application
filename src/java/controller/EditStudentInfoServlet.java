package controller;

import dao.StudentFacadeLocal;
import dao.StudentGroupFacadeLocal;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Student;
import model.StudentGroup;
import util.MyUtil;

/**
 *
 * @author hilshils
 * Данный сервлет предназначен для взаимодействия с пользователем
 * на странице (edit.jsp), где производится отображение формы редактирования/добавления.
 * Здесь производится обработка событий на странице отображения, 
 * обработка полученных данных от пользователя.
 * Данный сервлет позволяет добавлять новых студентов, а так же изменять существующих.
 * 
 */
@WebServlet(name = "EditStudentInfoServlet", urlPatterns = {"/EditStudentInfoServlet"})
public class EditStudentInfoServlet extends HttpServlet {

    @EJB
    private StudentFacadeLocal studentDao;
    @EJB
    private StudentGroupFacadeLocal groupDao;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Student student;
        String  action = request.getParameter("action");
        String  idStr  = request.getParameter("id");
        Long    id     = MyUtil.longFromString(idStr);
        
        if("save".equals(action)) {
            
            String courseStr      = request.getParameter("course");
            String groupNumberStr = request.getParameter("groupNumber");
            
            String name           = request.getParameter("name");
            String lastname       = request.getParameter("lastname");
            Long   course         = MyUtil.longFromString(courseStr);
            Long   groupNumber    = MyUtil.longFromString(groupNumberStr);
            
            StudentGroup group = (groupNumber != null)? groupDao.find(groupNumber) : null;
            student            = new Student(name, lastname, course, group);
            
            if(id != null) {
                student.setId(id);
                studentDao.edit(student);
            } else studentDao.create(student);
            
            response.sendRedirect("EditStudentInfoServlet?id=" + student.getId());
        } else {
            student = (id != null)? studentDao.find(id) : null;
            if(student != null)
                request.setAttribute("student", student);
            request.setAttribute("groups", groupDao.findAll());
            request.getRequestDispatcher("edit.jsp").forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
