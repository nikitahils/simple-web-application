package controller;

import dao.StudentFacadeLocal;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hilshils
 * Данный сервлет предназначен для взаимодействия с пользователем
 * на странице (search.jsp), где производится отображение формы поиска.
 * Здесь производится обработка событий на странице отображения, 
 * а так же обработка полученных данных от пользователя и передача их другому контроллеру.
 * 
 */
@WebServlet(name = "SearchStudentInfoServlet", urlPatterns = {"/SearchStudentInfoServlet"})
public class SearchStudentInfoServlet extends HttpServlet {

    @EJB
    private StudentFacadeLocal studentDao;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");
        if("search".equals(action)) {
            String type  = request.getParameter("type");
            String query = request.getParameter("query");
            StringBuilder builder = new StringBuilder("TableStudentInfoServlet");
            if(type != null) {
                builder.append("?type=").append(type);
                builder.append("&query=").append(query);
            }
            response.sendRedirect(builder.toString()); 
        } else {
            request.getRequestDispatcher("search.jsp").forward(request, response); 
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
